<?php

namespace DFM\Quote\Providers;

use Illuminate\Support\ServiceProvider;

class QuoteServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/menu-admin.php', 'menu.admin'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'dfm-quote');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'dfm-quote');

        $this->publishes([
            __DIR__.'/../../database/migrations/' => database_path('migrations')
        ], 'quote-migrations');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('lang/vendor/dfm-quote'),
        ], 'quote-lang');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/dfm-quote'),
        ], 'quote-views');
    }
}
