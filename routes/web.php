<?php

Route::middleware('admin', 'bindings')
    ->namespace('DFM\Quote\Http\Controllers\Admin')
    ->prefix('admin/sales')
    ->name('admin.sales.')
    ->group(function () {
        Route::resource('quotes', 'QuoteController')->only(['index', 'show']);

        Route::prefix('quotes')->name('quotes.')->group(function () {
            Route::post('/export', 'QuoteController@export')->name('export');
        });
    });
