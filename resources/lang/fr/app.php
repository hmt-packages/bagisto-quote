<?php

return [
    'datagrid'  => [
        'quote-date'    => 'Date du Devis',
    ],
    'layouts'   => [
        'quotes'        => 'Devis',
    ],
    'sales'     => [
        'quotes'        => [
            'title'             => 'Devis',
            'view-title'        => 'Devis #:quote_id',
            'quote-and-account' => 'Devis et Compte',
            'quote-info'        => 'Informations sur le Devis',
            'quote-date'        => 'Date du Devis',
            'shipping'          => 'Livraison',
            'products-quoted'   => 'Devis Produit',
        ],
    ],
];
