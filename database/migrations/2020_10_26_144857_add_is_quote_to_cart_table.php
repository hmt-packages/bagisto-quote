<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsQuoteToCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart', function (Blueprint $table) {
            if (! Schema::hasColumn('cart', 'is_quote')) {
                $table->boolean('is_quote')->after('is_active')->default(false);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart', function (Blueprint $table) {
            if (Schema::hasColumn('cart', 'is_quote')) {
                $table->dropColumn('is_quote');
            }
        });

    }
}
